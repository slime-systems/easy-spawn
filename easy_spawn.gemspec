# frozen_string_literal: true

require_relative 'lib/easy_spawn/version'

::Gem::Specification.new do |spec|
  spec.name = 'easy_spawn'
  spec.version = ::EasySpawn::VERSION
  spec.author = 'Sarun Rattanasiri'
  spec.email = 'midnight_w@gmx.tw'

  spec.summary = 'A legacy of POSIX::Spawn, providing easier interface for modern Rubies.'
  spec.description = 'TOD O: Write a longer description or delete this line.'
  # spec.homepage = "TOD O: Put your gem's website or public repo URL here."
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'

  spec.metadata['allowed_push_host'] = "TOD O: Set to your gem server 'https://example.com'"

  # spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TOD O: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TOD O: Put your gem's CHANGELOG.md URL here."

  spec.files = [
    *::Dir['lib/**/*'],
    'README.md',
    'LICENSE.md',
  ]
  spec.require_paths = ['lib']
  spec.add_dependency 'activesupport'
end
