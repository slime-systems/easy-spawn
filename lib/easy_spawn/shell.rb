# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/object/try'
require 'active_support/core_ext/time'
require 'open3'

module EasySpawn
  class Shell
    class Error < ::StandardError
    end

    class TimeoutError < Error
    end

    class P_FinishWrite < ::StandardError
    end

    P_CHUNK_SIZE = 1024 * 8

    private_constant :P_FinishWrite, :P_CHUNK_SIZE

    def initialize(timeout: nil)
      @timeout = timeout
      freeze
    end

    def call(*command, input: '', env: {}, options: {})
      out_buffer, err_buffer = [::StringIO.new, ::StringIO.new]
      start_time = ::Time.current
      options = options.tap do |o|
        break nil unless o.is_a?(::Hash)
      end
      ::Open3.popen3(env, *command, *[options].compact) do |in_w, out_r, err_r, thread|
        [in_w, out_r, err_r, out_buffer, err_buffer].each(&:binmode)
        read_queue, write_queue = [[out_r, err_r], [in_w]]
        input_buffer = input
        unless input_buffer.present?
          in_w.close
          write_queue.delete(in_w)
        end
        remaining_time = @timeout
        loop do
          raise TimeoutError if remaining_time.try(:<=, 0)
          readable, writable = ::IO.select(
            read_queue,
            write_queue,
            read_queue + write_queue,
            *[remaining_time].compact,
          )
          writable&.each do |io|
            # assume io == in_w
            byte_written = io.write_nonblock(input_buffer)
            input_buffer = input_buffer.byteslice(byte_written..-1)
            raise P_FinishWrite unless input_buffer
            raise P_FinishWrite unless 0 < input_buffer.bytesize
          rescue ::Errno::EAGAIN, ::Errno::EINTR
            next
          rescue ::Errno::EPIPE, P_FinishWrite
            io.close
            write_queue.delete(io)
          end
          readable&.each do |io|
            buffer = io.read_nonblock(P_CHUNK_SIZE)
            out_buffer << buffer if io == out_r
            err_buffer << buffer if io == err_r
          rescue ::Errno::EAGAIN, ::Errno::EINTR
            next
          rescue ::EOFError
            io.close
            read_queue.delete(io)
          end
          break unless read_queue.present? || write_queue.present?
          elapse = ::Time.current - start_time
          remaining_time = @timeout.try(:-, elapse)
        end
        [out_buffer.string, err_buffer.string, thread.value]
      rescue ::Object => e
        # clean up
        [in_w, out_r, err_r].each do |io|
            io.close
        rescue ::StandardError
            nil
        end
        begin
          ::Process.kill('TERM', thread.pid)
        rescue ::StandardError
          nil
        end
        begin
          ::Process.waitpid(thread.pid)
        rescue ::StandardError
          nil
        end
        raise e
      end
    ensure
      [out_buffer, err_buffer].each(&:close)
    end
  end
end
